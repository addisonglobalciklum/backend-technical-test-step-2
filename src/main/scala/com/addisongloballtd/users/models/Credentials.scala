package com.addisongloballtd.Models

case class Credentials(username: String, password: String)