package com.addisongloballtd.users.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSelection}
import com.addisongloballtd.Models.Credentials
import com.addisongloballtd.users.actors.Authenticate.{AuthenticateStatus, InvalidAuth, ValidAuth, Validate}
import com.addisongloballtd.users.actors.IssueToken.GenerateToken
import com.addisongloballtd.users.models.{InvalidUserToken, User}

import scala.util.Random

object Authenticate {
  sealed trait AuthenticateCommand
  case class Validate(credentials: Credentials, responseTo: ActorRef, tellTo: ActorSelection) extends AuthenticateCommand

  sealed trait AuthenticateStatus
  final case class ValidAuth(user: User)   extends AuthenticateStatus
  final case class InvalidAuth(user: User) extends AuthenticateStatus
}

//class Authenticate(issueTokenRef: ActorRef) extends Actor with ActorLogging {
class Authenticate extends Actor with ActorLogging {

  override def preStart(): Unit = {
    //log.info(s"Authenticate up")
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    log.info(s"Authenticate preRestart message : $message : $reason")
    super.preRestart(reason, message)
  }

  override def postRestart(reason: Throwable): Unit = {
    log.info(s"Authenticate postRestart $reason")
    super.postRestart(reason)
  }

  override def postStop(): Unit = {
    log.info(s"Authenticate stop")
  }

  def receive = {
    case Validate(credentials, responseTo, tellToIssueToken) => {
      log.info(s"auth actor Validate : $credentials : ${responseTo.path} : ${tellToIssueToken.pathString}")
      Thread.sleep(Random.nextInt(5000))
      validateCredentials(credentials) match {
        case ValidAuth(user)  => {
          log.info(s"auth actor send msg to issueToken")
          tellToIssueToken ! GenerateToken(User(credentials.username), responseTo)
        }
        case InvalidAuth(user) =>
          log.info(s"auth actor InvalidAuth $user : ${responseTo.path}")
          responseTo ! InvalidUserToken(user)
      }
    }
  }

  private def validateCredentials(credentials: Credentials) : AuthenticateStatus = {
    val isEqual  = credentials.username.toLowerCase.equals(credentials.password.toLowerCase)
    val upperPwd = credentials.password.exists(_.isLower)
    if(!upperPwd && isEqual)
      ValidAuth(User(credentials.username))
    else
      InvalidAuth(User(credentials.username))
  }

}
