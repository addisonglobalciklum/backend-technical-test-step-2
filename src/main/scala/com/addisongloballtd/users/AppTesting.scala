package com.addisongloballtd.users

import com.addisongloballtd.Models.Credentials
import com.addisongloballtd.users.services.SimpleAsyncTokenServiceProvider
import scala.concurrent.ExecutionContext.Implicits.global

object AppTesting extends App with SimpleAsyncTokenServiceProvider {

  println(s"AppTesting start")
  val userTokenF = simpleAsyncTokenService.requestToken(Credentials("Ale", "ALE"))
  val userTokenF2 = simpleAsyncTokenService.requestToken(Credentials("house2", "HOUSE2"))
  val userTokenF3 = simpleAsyncTokenService.requestToken(Credentials("house3", "HOUSE3"))
  val userTokenF4 = simpleAsyncTokenService.requestToken(Credentials("house4", "HOUSE4"))
  val userTokenF5 = simpleAsyncTokenService.requestToken(Credentials("house5", "HOUSE5"))
  val userTokenF6 = simpleAsyncTokenService.requestToken(Credentials("house6", "HOUSE6"))
  val userTokenF7 = simpleAsyncTokenService.requestToken(Credentials("house7", "HOUSE7"))
  val userTokenF8 = simpleAsyncTokenService.requestToken(Credentials("house8", "HOUSE8"))
  val userTokenF9 = simpleAsyncTokenService.requestToken(Credentials("house9", "HOUSE8"))
  val userTokenF10 = simpleAsyncTokenService.requestToken(Credentials("house10", "HOUSE8"))
  val userTokenF11 = simpleAsyncTokenService.requestToken(Credentials("house11", "HOUSE8"))
  val userTokenF12 = simpleAsyncTokenService.requestToken(Credentials("house12", "HOUSE8"))
  val userTokenF13 = simpleAsyncTokenService.requestToken(Credentials("house13", "HOUSE8"))
  val userTokenF14 = simpleAsyncTokenService.requestToken(Credentials("house14", "HOUSE8"))
  val userTokenF15 = simpleAsyncTokenService.requestToken(Credentials("house15", "HOUSE8"))

  userTokenF.map( userToken => println(s"AppTesting userToken :$userToken"))
  println(s"AppTesting end")

}
