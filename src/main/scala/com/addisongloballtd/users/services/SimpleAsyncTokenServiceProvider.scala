package com.addisongloballtd.users.services

trait SimpleAsyncTokenServiceProvider {
  def simpleAsyncTokenService : SimpleAsyncTokenService = SimpleAsyncTokenServiceImpl
}
