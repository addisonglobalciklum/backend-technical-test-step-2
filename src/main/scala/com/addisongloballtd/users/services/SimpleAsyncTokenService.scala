package com.addisongloballtd.users.services

import com.addisongloballtd.Models.Credentials
import com.addisongloballtd.users.models.UserToken

import scala.concurrent.Future

trait SimpleAsyncTokenService {
  def requestToken(credentials: Credentials): Future[UserToken]
}
