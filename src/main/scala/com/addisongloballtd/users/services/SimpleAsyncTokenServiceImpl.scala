package com.addisongloballtd.users.services

import akka.actor.{ActorSystem, Props}
import com.addisongloballtd.Models.Credentials
import com.addisongloballtd.users.actors.Authenticate.Validate
import com.addisongloballtd.users.actors.{Authenticate, IssueToken, UserSupervisor}
import com.addisongloballtd.users.models.{User, UserToken}
import akka.pattern.ask
import akka.routing.RoundRobinPool
import akka.util.Timeout
import com.addisongloballtd.users.actors.UserSupervisor.{FindToken, ProcessToken}

import scala.concurrent.{Await, Future}

trait SimpleAsyncTokenServiceImpl extends SimpleAsyncTokenService with SimpleAsyncTokenActorSystemProvider{

  import scala.concurrent.duration._
  implicit val timeout = Timeout(11000.millisecond)

  def requestToken(credentials: Credentials): Future[UserToken] = {
    val actorSystem = simpleAsyncTokenActorSystem.startActors()

    val issueToken   = actorSystem.actorSelection("/user/issue-token-pool")
    val authenticate = actorSystem.actorSelection("/user/auth-pool")
    val supervisor   = actorSystem.actorSelection("/user/supervisor-pool")

    (supervisor ? ProcessToken(credentials, authenticate, issueToken)).mapTo[UserToken]
  }

}

object SimpleAsyncTokenServiceImpl extends SimpleAsyncTokenServiceImpl