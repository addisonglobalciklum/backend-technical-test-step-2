package com.addisongloballtd.users.services

trait SimpleAsyncTokenActorSystemProvider {
  def simpleAsyncTokenActorSystem : SimpleAsyncTokenActorSystem = SimpleAsyncTokenActorSystem
}
