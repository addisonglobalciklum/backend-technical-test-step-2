package com.addisongloballtd.users.data

import com.addisongloballtd.Models.Credentials
import com.addisongloballtd.users.models.User

trait UserData {
  val credentials = Credentials("house", "HOUSE")
  val user        = User(credentials.username)

  val invalidCredentials = Credentials("house", "HOUSe")
  val invalidUser        = User(invalidCredentials.username)

  val credentialsStartA = Credentials("Ahouse", "HOUSE")
  val userStartA        = User(credentialsStartA.username)

}
object UserData extends UserData
