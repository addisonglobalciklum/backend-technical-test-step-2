# 2. Service Implementation

Provide an implementation for the following API, which is different from the one designed in the previous section:

```bash
 trait SimpleAsyncTokenService {
   def requestToken(credentials: Credentials): Future[UserToken]
 }
```

**Task**: We prefer you to use an Actor Model implementation such as Akka, but it's not mandatory. You can use other frameworks like Spring or any other of your choice.

##### 1. Implement an Actor/Service/Module that:

This business logic is represented in an Actor called **Authenticate**

##### 2. Implement another Actor/Service/Module that:

This business logic is represented in an Actor called **IssueToken**
         
##### 3. Implement the requestToken function/method from the SimpleAsyncTokenService trait/interface in a way that:
         
This is the service that send a message to supervisor actor using Ask pattern to wait for async response

##### Technical Consideration:

- I choose Akka toolkit, because It's implement Actor Model following reactive manifest to attend the requirements of this exercice. Akka was created to manage highly concurrency and fault tolerant.
- UserSupervisor: This actor has the responsability to orchestrate Authenticate and IssueToken Actor, manage exceptions overriding supervisorStrategy.
- SimpleAsyncTokenActorSystem: This service has the responsability to create actor system and create an actor's random pool to increase concurrency and non-blocking request. You could increase the actors pool in application.conf.
- AppTest: This is the main app that do a request to simpleAsyncTokenService.


## Running

Run this using [sbt](http://www.scala-sbt.org/).  If you downloaded this project from <http://www.playframework.com/download> then you'll find a prepackaged version of sbt in the project directory:

```bash
sbt run
```

## Testing
```bash
sbt test
```